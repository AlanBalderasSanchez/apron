package com.apron.models;

public class Item {

	private String itemName;
	private Integer cost;
	
	public Item(String name) {
		this.itemName = name;
	}
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}

	
}
