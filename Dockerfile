FROM openjdk:14-alpine
COPY build/libs/Apron-*-all.jar Apron.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "Apron.jar"]