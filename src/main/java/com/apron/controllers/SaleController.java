package com.apron.controllers;

import java.util.ArrayList;
import java.util.List;

import com.apron.models.Item;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/")
public class SaleController {

	@Get
	public List<Item> getItems(){
		
		List<Item> items = new ArrayList<>();
		
		items.add(new Item("penzil"));
		items.add(new Item("notebook"));
		items.add(new Item("pen"));
		
		return items;
	}
	
	
	
}
